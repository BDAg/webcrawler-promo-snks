import telebot
import requests
from bs4 import BeautifulSoup

CHAVE_API = "Insira sua chave"

bot = telebot.TeleBot(CHAVE_API)




def obter_promocoes():
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_115) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'
    }
    pagina = requests.get('https://www.maze.com.br/categoria/sale/tenis', headers=headers)
    conteudo = pagina.content
    site = BeautifulSoup(conteudo, 'html.parser')
    body = site.find("body")
    grid = body.find("div", class_="type-grid")
    product_container = grid.find_all("div", class_="content")
    produtos = []

    for produto in product_container:
        title = produto.find_all('span')[0].text
        old_price = produto.find('span', class_="precoBase").text
        price = produto.find_all('span')[3].text
        link = produto.find('a')['href'].strip()

        mensagem = f'Produto: {title.strip()}\nPreço antigo: {old_price}\nPreço Atual R$: {price}\nLink: maze.com.br{link}\n'
        produtos.append(mensagem)

    return produtos

@bot.message_handler(commands=['promo'])
def opcao2(a):
  for item in obter_promocoes():
    bot.reply_to(a, item)


def verificar(mensagem):
    return True

@bot.message_handler(func=verificar)
def responder(mensagem):
    texto = '''
    Olá, eu sou o Bot Promo SNKRS! Escolha uma opção para continuar (Clique no item):
    /promo Promoções
    Responda qualquer outra coisa, e não vai funcionar, clique na opção 1
    '''
    bot.reply_to(mensagem, texto)

bot.polling()
